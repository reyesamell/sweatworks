//
//  RandomUserHTTPClient.h
//  Prueba
//
//  Created by Hugo Reyes on 3/3/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
NS_ASSUME_NONNULL_BEGIN

@protocol RandomUserHTTPClientDelegate;

@protocol RandomUserHTTPClientDelegate <NSObject>
@optional
// Method to be called with the client gets a response from server
-(void)notifyHTTPResponse: (id)anObject : (Boolean)error;
@end


@protocol RandomUserHTTPClientProtocol <NSObject>
@optional
// Method to be called with the client gets a response from server
- (void)requestUsersWithParams:(NSString *)stringParams;
@end

@interface RandomUserHTTPClient: AFHTTPSessionManager <RandomUserHTTPClientProtocol>
@property (nonatomic, weak) id<RandomUserHTTPClientDelegate>delegate;

// Singleton for RandomUserHTTPClient
+(RandomUserHTTPClient *)sharedInstance;
- (void)requestUsersWithParams:(NSString *)stringParams;
@end



NS_ASSUME_NONNULL_END
