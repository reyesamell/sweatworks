//
//  RandomUserHTTPClient.m
//  Prueba
//
//  Created by Hugo Reyes on 3/3/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

#import "RandomUserHTTPClient.h"
#import "Constants.h"

@implementation RandomUserHTTPClient


+ (RandomUserHTTPClient * )sharedInstance {
    // Verify is there is already an instance of randomUserHTTPClient,
    // Otherwise a new one is created.
    static RandomUserHTTPClient *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] initWithBaseURL:([NSURL URLWithString:BASE_URL])];
        
    });
    return sharedInstance;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    return self;
}


- (void)requestUsersWithParams:(NSString *)stringParams{
    // It is not beautiful for me sending http requests params as a single string
    // but that was the easiest way I found it to implement the network layer in
    // objectiv-c considering the given time and my time availability.
    
    NSString *string = [NSString stringWithFormat:@"%@%@", BASE_URL , stringParams];
    
    
    [RandomUserHTTPClient.sharedInstance GET:string parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"Success");
        if (self.delegate){
            // If there is any object delegated, send the new object.
            [self.delegate notifyHTTPResponse:responseObject :false];
        }else{
            // This section should not be run, since the normal way to use
            // the client is via an object and using delegates.
            
            // Just in case, the function returns and nothing happens
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        id responseObject = nil;
        // Send to delegate the error found
        [self.delegate notifyHTTPResponse:responseObject :true];
    }];
}





@end
