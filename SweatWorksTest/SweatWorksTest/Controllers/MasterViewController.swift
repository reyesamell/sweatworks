//
//  MasterViewController.swift
//  SweatWorksTest
//
//  Created by Hugo Reyes on 3/4/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

import UIKit
import SDWebImage

class MasterViewController: UIViewController {

    
    // Properties
    var model: [User] = []
    let modelView = MasterViewControllerModelView()
    let searchBarView = SearchBarView()
    @IBOutlet weak var searchBarViewSpace: UIView!
    @IBOutlet weak var tableview: UITableView!
    var detailViewController: DetailViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addKeyBoardListener()
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self) //remove observer
    }

    func setupViews(){
        self.modelView.delegate = self
        self.modelView.customInitialize()
        
        self.searchBarViewSpace.addSubview(self.searchBarView)
        self.searchBarView.snp.makeConstraints { (make) in
            make.width.height.equalToSuperview()
            make.center.equalToSuperview()
        }
        
        self.searchBarView.setupLayouts()
        self.searchBarView.searchTV.delegate = self
        self.searchBarView.searchTV.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshUsers), for: .valueChanged)
        refreshControl.beginRefreshing()
        
        self.searchBarView.cancelButton.addTarget(self, action: #selector(self.cancelSearch), for: .touchUpInside)
        self.searchBarView.eraseButton.addTarget(self, action: #selector(self.eraseSearchWord), for: .touchUpInside)
        
        self.tableview.refreshControl = refreshControl
        self.tableview.tableFooterView = UIView()
    }
    
    @objc func eraseSearchWord(){
        // X button action, deletes textfield text.
        self.searchBarView.searchTV.text = ""
        self.modelView.searchWord = ""
        self.modelView.shouldShowLoadingCell = false
        
    }
    
    @objc func cancelSearch(){
        // First errase text from searchBarView
        self.searchBarView.searchTV.text = nil
        // Resign keyboard
        self.searchBarView.searchTV.endEditing(true)
        self.modelView.searchWord = ""
        self.modelView.shouldShowLoadingCell = true
        self.tableview.reloadData()
    }
    
    
    
    @objc func refreshUsers(){
        if (self.searchBarView.searchTV.text == "") || (self.searchBarView.searchTV.text == nil){
            
            self.modelView.pageNumber = 1
            self.modelView.requestPage(page: 1)
            return
        }else{
            self.tableview.refreshControl?.endRefreshing()
        }
        
    }
    
    
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            
            if let indexPath = self.tableview.indexPathForSelectedRow {
                
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
            
                controller.address = self.model[indexPath.row].location.street + ", " + self.model[indexPath.row].location.city + ", " + self.model[indexPath.row].location.state
                
                controller.imageURL = self.model[indexPath.row].picture.large
                
                controller.name = self.model[indexPath.row].name.last + ", " + self.model[indexPath.row].name.first
                
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
               
            }
        }
    }



}


extension MasterViewController: UpdateModelDelegate{
    func updateModel(filteredUsers: [User]) {
        self.tableview.refreshControl?.endRefreshing()
        self.model = filteredUsers
        self.tableview.reloadData()
    }
    
    func reportNetworkError() {
        self.tableview.refreshControl?.endRefreshing()
        
        let alert = UIAlertController(title: NSLocalizedString("networkErrorTitle", comment: "Title of network error alert"), message: NSLocalizedString("networkErrorDescription", comment: "Networ error description"), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("okLabel", comment: "Ok"), style: .default, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}


extension MasterViewController: UITextFieldDelegate{
    // Mark: Keyboard
    func addKeyBoardListener() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        self.searchBarView.backgroundView.snp.updateConstraints { (make) in
            make.right.equalToSuperview().offset(-70)
        }
        
        self.searchBarView.cancelButton.isHidden = false
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.searchBarView.backgroundView.snp.updateConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            self.searchBarView.cancelButton.isHidden = true
        }
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        if (self.searchBarView.searchTV.text == "") || (self.searchBarView.searchTV.text == nil){
            self.modelView.shouldShowLoadingCell = true
        }
        
        self.tableview.reloadData()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.modelView.shouldShowLoadingCell = false
        guard let text = textField.text else{return}
        
        self.modelView.searchWord = text.lowercased()
    }
    
}

extension MasterViewController: UITableViewDelegate, UITableViewDataSource{
    // MARK: - Table View
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetail", sender: self)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard self.modelView.shouldShowLoadingCell else { return false }
        return indexPath.row == self.model.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        // guard self.model.count > 0 else {return 1}
        let count = self.model.count
        return self.modelView.shouldShowLoadingCell ? count + 1 : count
        //return count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        
        if isLoadingIndexPath(indexPath) {
            return LoadingCellTableViewCell(style: .default, reuseIdentifier: "loading")
        } else {
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RandomUserUITableViewCell
            
            cell.name!.text = self.model[indexPath.row].name.last + ", " + self.model[indexPath.row].name.first
            
            guard let url = URL(string: self.model[indexPath.row].picture.medium) else {return cell}
            
            cell.mainImage.sd_setImage(with: url) { (image, error, sdImageCache, url) in
            }
            
            cell.mainImage.layer.cornerRadius = cell.mainImage.frame.size.width / 2;
            
            cell.mainImage.clipsToBounds = true;
            
            cell.phone.text = self.model[indexPath.row].phone
            
            cell.accessoryType = .disclosureIndicator
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isLoadingIndexPath(indexPath) else { return }
          if let number = self.modelView.pageNumber{
              self.modelView.pageNumber = number + 1
         }
        self.modelView.requestPage(page: self.modelView.pageNumber!)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    

}
