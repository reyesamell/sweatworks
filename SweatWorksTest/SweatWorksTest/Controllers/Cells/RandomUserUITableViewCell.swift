//
//  RandomUserUITableViewCell.swift
//  SweatWorksTest
//
//  Created by Hugo Reyes on 3/4/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

import UIKit

class RandomUserUITableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
