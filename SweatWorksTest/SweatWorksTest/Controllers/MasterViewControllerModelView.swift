//
//  MasterViewControllerModelView.swift
//  SweatWorksTest
//
//  Created by Hugo Reyes on 3/4/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

import Foundation

class MasterViewControllerModelView: UIViewController, RandomUserHTTPClientDelegate{
    // Properties:
    var pageNumber: Int? // Pagination for data from api
    var shouldShowLoadingCell = true // Flag for loading cell for pagination
    var  httpClient : RandomUserHTTPClient?
    var delegate: UpdateModelDelegate?
    
    var users: [User] = [] {
        didSet{
            
            if delegate != nil {
                // Filter information
                let filteredUsers = filterByWordFromUser(listOfObjects: self.users, word: self.searchWord)
                self.delegate?.updateModel(filteredUsers: filteredUsers)
            }
        }
    }
    
    var searchWord : String = ""{
        didSet{
            let filteredUsers = filterByWordFromUser(listOfObjects: self.users, word: self.searchWord)
            self.delegate?.updateModel(filteredUsers: filteredUsers)
        }
    }

    
    func customInitialize(){
        self.searchWord = ""
        self.users = []
        self.httpClient = RandomUserHTTPClient.sharedInstance()
        self.httpClient?.delegate = self
        self.pageNumber = 1
        self.requestPage(page: self.pageNumber!)
    }
    
    func requestPage(page: Int){
        // This method is called by the controller to request data from server.
        // In consequence, this method sends the info to the real network layer.
        // The network layer trigger another method in this controller in which is triggers subsequently the final method for the last destination.
        
        // Normally I would not do it in this way, I did it just for the objective c
        // for the network layer.
        let numberUsersPerPage = 30
        let seedRequestedByWebAPI = "abc"
        let string = "?page=\(page)&results=\(numberUsersPerPage)&seed=\(seedRequestedByWebAPI)"
        // example of what is desired for this string:
        // "?page=100&results=2&seed=abc"
        guard self.httpClient != nil else{
            if self.delegate != nil {
                self.delegate?.reportNetworkError()
            }
            return
        }
        self.httpClient?.requestUsers(withParams: string)
    }
    
    
    
    func notifyHTTPResponse(_ anObject: Any, _ error: Bool) {
        // This method is trigger when new information is received from the real
        // network layer, then, the information is sent to the final controller via
        // updateModelDelegate.
        
        guard !error else{
            if delegate != nil {
                self.delegate?.reportNetworkError()
            }
         
            return}
        do{
            let dataJson = try JSONSerialization.data(withJSONObject: anObject, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            let dataFromWeb = try JSONDecoder().decode(ResponseRandomUsers.self, from: dataJson)
            
            if let number = self.pageNumber{
                if number == 1{
                self.users = dataFromWeb.results.sorted { (id1, id2) -> Bool in
                    return id1.name.last < id2.name.last // Use > for Descending order
                    }
                }else{
                    let results = dataFromWeb.results.sorted { (id1, id2) -> Bool in
                        return id1.name.last < id2.name.last // Use > for Descending order
                    }
                    
                    for index in 0...results.count - 1{
                        self.users.append(results[index])
                    }
                    
//                    self.users = self.users.sorted { (id1, id2) -> Bool in
//                        return id1.name.last < id2.name.last // Use > for Descending order
//                    }
                }
            }
        
        } catch let _ {
            
            self.delegate?.reportNetworkError()
        }
        
    }
    
    
    func filterByWordFromUser(listOfObjects: [User], word: String) -> [User]{
        let searchWord = "^" + word + "[A-Za-z]"
        return listOfObjects.filter( {$0.name.last ~= searchWord })
    }

}



