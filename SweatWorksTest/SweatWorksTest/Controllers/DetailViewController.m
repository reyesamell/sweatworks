//
//  DetailViewController.m
//  T2
//
//  Created by Hugo Reyes on 3/4/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

#import "DetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
       // self.detailDescriptionLabel.text = [self.detailItem description];
    }
    if (self.name){
        self.nameLabel.text = [self name];
    }
    
    if (self.address){
        self.addressLabel.text = [self address];
    }
    
    if (self.imageURL){
        NSURL *url = [NSURL URLWithString: [self imageURL]];
        if (url){
            [[self mainImage] sd_setImageWithURL:url completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                self.mainImage.layer.cornerRadius = self.mainImage.frame.size.width / 2;
                
                self.mainImage.clipsToBounds = true;

            }];
            
        }
        /*
         
         */

    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}


#pragma mark - Managing the detail item

- (void)setDetailItem:(NSDate *)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}


@end
