//
//  User.swift
//  SweatWorksTest
//
//  Created by Hugo Reyes on 3/4/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

import Foundation
struct Picture: Codable{
    var large: String
    var medium: String
    var thumbnail: String
    
    enum CodingKeys: String, CodingKey{
        case large
        case medium
        case thumbnail
    }
}
struct Name: Codable {
    var first: String
    var last: String
    
    enum CodingKeys: String, CodingKey{
        case first
        case last
    }
    
}

struct Location: Codable {
    var street: String
    var city: String
    var state: String
    
    enum CodingKeys: String, CodingKey{
        case street
        case city
        case state
    }
}

struct User: Codable {
    
    var phone: String
    var name: Name
    var picture: Picture
    var location: Location
    enum CodingKeys: String, CodingKey{
        case phone
        case name
        case picture
        case location
    }
}

struct ResponseRandomUsers: Codable{
    
    var results: [User]
    
    enum CodingKeys: String, CodingKey{
        case results
    }
    
}

