//
//  Protocols.swift
//  SweatWorksTest
//
//  Created by Hugo Reyes on 3/7/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

import Foundation

protocol UpdateModelDelegate{
    /*
     It  is used to transmit data from the modelview data handler (MVVM pattern) to the ViewController (view).
     */
    func updateModel(filteredUsers: [User])
    func reportNetworkError()
}

