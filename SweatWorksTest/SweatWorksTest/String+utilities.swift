//
//  String+utilities.swift
//  SweatWorksTest
//
//  Created by Hugo Reyes on 3/7/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

import Foundation

extension String{
    func createAttributeString(font: UIFont, color: UIColor, spacing: CGFloat = 4, letterSpacing: CGFloat = 1, aligment: NSTextAlignment = .left) -> NSMutableAttributedString{
        
        //let style = NSMutableParagraphStyle()
        //style.alignment = aligment
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = aligment
        paragraphStyle.lineSpacing = CGFloat(spacing)
        let attributedString = NSMutableAttributedString(string: self, attributes:
            [.font: font,
             .paragraphStyle: paragraphStyle,
             .foregroundColor: color,
             .kern: letterSpacing])
        
        return attributedString
        
    }
    
    static func ~= (lhs: String, rhs: String) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: rhs) else { return false }
        let range = NSRange(location: 0, length: lhs.utf16.count)
        return regex.firstMatch(in: lhs, options: [], range: range) != nil
    }
}

