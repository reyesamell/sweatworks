//
//  SearchBarView.swift
//  SweatWorksTest
//
//  Created by Hugo Reyes on 3/4/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

import UIKit
import SnapKit

class SearchBarView: UIView {
    
    lazy var backgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    
   let searchTV: CustomTextView = CustomTextView(size: 14, placeholder: "Usuario")

    let searchButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    let searchImageView : CustomImageView  = CustomImageView(image: UIImage(named: "baseline_search_black_24pt")!)
    
    let eraseButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let erraseImageView : CustomImageView  = CustomImageView(image: UIImage(named: "baseline_highlight_off_black_24pt")!)
    
    let cancelButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        let font = UIFont(name: "Georgia", size: 15)
        button.setAttributedTitle(NSLocalizedString("cancelButton", comment:
            "Button label").createAttributeString(font: font!, color: .black ), for: .normal)
        
        return button
    }()
    
  
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init() {
        super.init(frame: .zero)
        
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func setupLayouts(){
        self.layoutIfNeeded()
//        
        self.addSubview(self.backgroundView)
        self.backgroundView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.8)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        
        self.backgroundView.backgroundColor = .white

        self.backgroundView.layer.cornerRadius = 15
        self.backgroundView.clipsToBounds = true
        
        self.backgroundView.addSubview(self.searchButton)
        self.searchButton.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.5)
            make.width.equalTo(self.snp.height).multipliedBy(0.5)
            make.left.equalToSuperview().offset(5)
        }

        self.searchButton.addSubview(self.searchImageView)
        self.searchImageView.snp.makeConstraints { (make) in
            make.center.height.width.equalToSuperview()
        }
        
        self.backgroundView.addSubview(self.eraseButton)
        self.eraseButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.searchImageView)
            make.height.equalToSuperview().multipliedBy(0.4)
            make.width.equalTo(self.snp.height).multipliedBy(0.4)
            make.right.equalToSuperview().offset(-5)
        }
        
        self.eraseButton.addSubview(self.erraseImageView)
        self.erraseImageView.snp.makeConstraints { (make) in
            make.center.height.width.equalToSuperview()
        }
        
        self.backgroundView.addSubview(self.searchTV)
        self.searchTV.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.95)
            
            make.left.equalTo(self.searchImageView.snp.right)
            
            make.right.equalTo(self.erraseImageView.snp.left)
            
        }
        
        
        self.erraseImageView.contentMode = .scaleToFill

        self.addSubview(self.cancelButton)
        self.cancelButton.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.height.right.equalToSuperview()
            make.right.equalToSuperview()
            make.left.equalTo(self.backgroundView.snp.right)
        }
     
        self.cancelButton.isHidden = true
    }
    
}
