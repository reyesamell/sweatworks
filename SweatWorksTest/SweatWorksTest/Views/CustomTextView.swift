//
//  CustomTextView.swift
//  SweatWorksTest
//
//  Created by Hugo Reyes on 3/4/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//


import UIKit

class CustomTextView: UITextField {
    
    init(text: String = "", size: CGFloat = 18.0, textColor: UIColor = UIColor.gray, letterSpacing: CGFloat = 0, lineSpacing: CGFloat = 4, aligment: NSTextAlignment = NSTextAlignment.left, placeholder: String, bottomLine: Bool = false){
        super.init(frame: CGRect.zero)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let font = UIFont(name: "Georgia", size: size)
        self.font = font!
        self.textColor = UIColor.gray
        self.backgroundColor = UIColor.clear
        self.autocapitalizationType = UITextAutocapitalizationType.sentences
  
        
        
        
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        
        //let text = NSAttributedString(string: text ,attributes: [.paragraphStyle: style])
       // self.attributedText = text
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
}
