//
//  CustomImageView.swift
//  SweatWorksTest
//
//  Created by Hugo Reyes on 3/4/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

import UIKit

class CustomImageView: UIImageView {
    // LogoCategoryc class
    
    init(image: UIImage = UIImage()){
        // This line is needed to run the super class constructor.
        // The frame is set to zero, because the size of the class
        // will be changed in the future
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        self.setImage(image: image)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setImage(image: UIImage){
        self.image = image.withRenderingMode(.alwaysOriginal)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
