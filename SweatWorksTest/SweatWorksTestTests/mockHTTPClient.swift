//
//  UnitTestHTTPClient.swift
//  SweatWorksTestTests
//
//  Created by Hugo Reyes on 3/6/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

import Foundation
@testable import SweatWorksTest

class MockHTTPClient: NSObject, RandomUserHTTPClientProtocol{
    var delegate : RandomUserHTTPClientDelegate?
    var shoudRespondError = false // CHANGE HERE FOR SIMULATE A NIL RESPONSE FROM SERVER
    func requestUsersWithParams(withParams: String){
        // This method emulates a request to the api and the response
        if delegate != nil{
            
            // Create a json in order to simulate a data received from the request to a remote server.
 
            /*
             
             CHANGE HERE ANY KEY IN THE JSON FOR GENERATE AN UNEXPECTED RESPONSE, SEE THAT ONLY A PERFECT RESPONSE REACH THE TEST SUCESS.
            
             */
            let jsonString :[String:Any] = ["results":[[
                "name": [
                    "first": "gabriel",
                    "last": "molina"
                ],
                "phone":"222222-22",
                "location": [
                    "street": "9239 elm st",
                    "city": "Patterson",
                    "state": "NJ"
                ],
                "picture":["large":"urllarge", "medium": "urlmedium"
                    ,"thumbnail": "urlsmall"]
                ]]
            ]
            
                self.delegate!.notifyHTTPResponse!(jsonString, shoudRespondError)
            
        }
    }
    
}
