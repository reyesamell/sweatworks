//
//  SweatWorksTestTests.swift
//  SweatWorksTestTests
//
//  Created by Hugo Reyes on 3/4/19.
//  Copyright © 2019 Hugo Reyes. All rights reserved.
//

import XCTest
@testable import SweatWorksTest

class SweatWorksTestTests: XCTestCase, RandomUserHTTPClientDelegate {
    let httpClient = MockHTTPClient()
    
    private var numbersExpectation: XCTestExpectation!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


    func notifyHTTPResponse(_ anObject: Any, _ error: Bool) {
        guard !error else{
            numbersExpectation.fulfill()
            XCTFail("Problem with server response")
            return
        }
        do{
            
            let dataJson = try JSONSerialization.data(withJSONObject: anObject, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            _ = try JSONDecoder().decode(ResponseRandomUsers.self, from: dataJson)
            numbersExpectation.fulfill()
            
        } catch _ {
            numbersExpectation.fulfill()
            XCTFail()
        }
    
        
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        self.httpClient.delegate = self
        numbersExpectation = expectation(description: "Numbers")
        
        self.httpClient.requestUsersWithParams(withParams: "")
        waitForExpectations(timeout: 20)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
